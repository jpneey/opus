$(document).ready(function(){
    newsletterCard();
    getDate();
    
    
    modal();
    sideNav();
    logoNav();

    galModal();

    
    
});

$(document).ready(function () {
    $("#navbar[data-includeHTML]").each(function () {                
        $(this).load($(this).attr("data-includeHTML"));
    });
})

function newsletterCard() {
    let resize_card = $('div.three-col.card').outerHeight();
    let resize_four_col = $('div.four-col.card').outerHeight();
    $("#newsletter-card").css({"height" : '380px'});
    $("#four-col-card").css({"height" : ''+resize_four_col});
}
function galModal() {
    $('.gallery-item-four > img').on('click', function(){
        let src = $(this).attr('src');
        $('.image-pop > img').attr('src', src);
        $('.image-pop').show(100);
    })
    $('.image-pop').on('click', function(){
        $(this).hide(100);
    })
}

function getDate() {
    var d = new Date();
    var output = d.getFullYear();
    $('span.date').text(""+output);
}
function modal(e) {
    if (e == 1) {
        $('.modal').show();
    }
    else {
        $('.modal').hide(100);
    }
}

function sideNav(e) {
    var el = '.mobile-sidenav';
    if (e == 1) {
        $(el).show(100);
    }
    else {
        $(el).hide(100);
    }
}

function logoNav() {
    
    var f = "img.nav-logo";
    $(f).on('click', function(){

        window.location.replace("./index.html");
    })
}
function redir(e) {
    window.location.href = e;
}


$(document).on('click', 'a[href^="#"]', function (event) {
    event.preventDefault();

    $('html, body').animate({
        scrollTop: $($.attr(this, 'href')).offset().top
    }, 700);
});


