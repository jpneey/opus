$(function() {
    
    $('#news-btn-load').hide();
    $('.ajax-form').on('submit', function(e) {

        var url = $(this).attr('action');
        var data = $(this).serialize();

        $('#news-btn').hide();
        $('#news-btn-load').show();
        $('.submit-btn').val('sending . . .');
        e.preventDefault();
        $.ajax({
            type: 'post',
            url: url,
            data: data,
            success: function () {
            
                $('#news-btn-load').hide();
                $('#news-btn').show();
                alert('Thank you for subscribing!');
                $('.submit-btn').val('Message sent');
                
            }
        });
    });
});